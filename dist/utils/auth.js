"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const argon2 = __importStar(require("argon2"));
const User_1 = require("../entity/User");
const register = (username, password, number, name, secretTeacher) => __awaiter(void 0, void 0, void 0, function* () {
    const user = yield User_1.User.findOne({ username });
    if (user) {
        throw new Error("Username already taken");
    }
    try {
        const passwordHash = yield argon2.hash(password);
        yield User_1.User.insert({
            username,
            password: passwordHash,
            name,
            number,
            isTeacher: secretTeacher === 'magic'
        });
        console.log(`auth/register: ${username} created`);
        return true;
    }
    catch (e) {
        console.log("auth/register: ", e);
        return false;
    }
});
exports.register = register;
const login = (username, password) => __awaiter(void 0, void 0, void 0, function* () {
    const user = yield User_1.User.findOne({ username });
    if (!user) {
        throw new Error("Wrong Credentials");
    }
    const comparePasswords = yield argon2.verify(user.password, password);
    if (!comparePasswords) {
        console.log(`auth/login: ${user.username} attempted to log in`);
        throw new Error("Wrong Credentials");
    }
    console.log(`auth/login: ${user.username} logged in`);
    return user;
});
exports.login = login;
//# sourceMappingURL=auth.js.map