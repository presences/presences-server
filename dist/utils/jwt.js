"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = require("jsonwebtoken");
const createAccessToken = (user) => {
    return jsonwebtoken_1.sign({ userId: user.id, username: user.username, name: user.name }, process.env.accessTokenSecret || "testingAccess", {
        expiresIn: "15m"
    });
};
exports.createAccessToken = createAccessToken;
const createRefreshToken = (user, rememberMe) => {
    return jsonwebtoken_1.sign({
        userId: user.id,
        username: user.username,
        name: user.name,
        tokenVersion: 1
    }, process.env.refreshTokenSecret || "testingRefresh", {
        expiresIn: rememberMe ? "365d" : "7d"
    });
};
exports.createRefreshToken = createRefreshToken;
const sendRefreshToken = (res, token) => {
    res.cookie("jid", token, {
        httpOnly: true,
        path: "/refresh_token"
    });
};
exports.sendRefreshToken = sendRefreshToken;
//# sourceMappingURL=jwt.js.map