"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = require("jsonwebtoken");
const isAuthenticated = ({ context }, next) => {
    const authorization = context.req.headers["authorization"];
    if (!authorization)
        throw new Error("Not authenticated");
    try {
        const token = authorization.split(" ")[1];
        const payload = jsonwebtoken_1.verify(token, process.env.accessTokenSecret || "testingAccess");
        context.payload = payload;
    }
    catch (err) {
        console.log("middleware/graphql/auth/isAuthenticated: ", err);
        throw new Error("Not authenticated");
    }
    return next();
};
exports.isAuthenticated = isAuthenticated;
//# sourceMappingURL=auth.js.map