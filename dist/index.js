"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require('dotenv').config();
require("reflect-metadata");
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const cookie_parser_1 = __importDefault(require("cookie-parser"));
const jsonwebtoken_1 = require("jsonwebtoken");
const typeorm_1 = require("typeorm");
const User_1 = require("./entity/User");
const apollo_1 = __importDefault(require("./apollo"));
const jwt_1 = require("./utils/jwt");
const PORT = process.env.PORT || 5000;
(() => __awaiter(void 0, void 0, void 0, function* () {
    const app = express_1.default();
    app.use(cookie_parser_1.default());
    app.get("/", (_, res) => res.send("home"));
    yield typeorm_1.createConnection();
    app.use(cors_1.default({
        origin: "http://localhost:3000",
        credentials: true
    }));
    app.post("/refresh_token", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        if (!req.cookies) {
            return res.send({ ok: false, accessToken: "" });
        }
        const token = req.cookies.jid;
        if (!token) {
            return res.send({ ok: false, accessToken: "" });
        }
        let payload = null;
        try {
            payload = jsonwebtoken_1.verify(token, process.env.refreshTokenSecret || "testingRefresh");
        }
        catch (err) {
            console.log(err);
            return res.send({ ok: false, accessToken: "" });
        }
        const user = yield User_1.User.findOne({ id: payload.userId });
        if (!user) {
            return res.send({ ok: false, accessToken: "" });
        }
        jwt_1.sendRefreshToken(res, jwt_1.createRefreshToken(user, false));
        return res.send({ ok: true, accessToken: jwt_1.createAccessToken(user) });
    }));
    const apolloServer = yield apollo_1.default();
    apolloServer.applyMiddleware({ app, cors: false, path: "/api" });
    app.listen(PORT, () => {
        console.log(`Express server started on port ${PORT} 🚀`);
    });
}))();
//# sourceMappingURL=index.js.map