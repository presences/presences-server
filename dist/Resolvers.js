"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const User_1 = require("./entity/User");
const type_graphql_1 = require("type-graphql");
const auth_1 = require("./utils/auth");
const jwt_1 = require("./utils/jwt");
const auth_2 = require("./middleware/graphql/auth");
let LoginResponse = class LoginResponse {
};
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], LoginResponse.prototype, "accessToken", void 0);
LoginResponse = __decorate([
    type_graphql_1.ObjectType()
], LoginResponse);
let MeResponse = class MeResponse {
};
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], MeResponse.prototype, "username", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], MeResponse.prototype, "name", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", Number)
], MeResponse.prototype, "number", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", Boolean)
], MeResponse.prototype, "isTeacher", void 0);
MeResponse = __decorate([
    type_graphql_1.ObjectType()
], MeResponse);
let Resolvers = class Resolvers {
    hello() {
        return "hello";
    }
    users() {
        return User_1.User.find();
    }
    me({ payload }) {
        return __awaiter(this, void 0, void 0, function* () {
            const { username } = payload;
            const user = yield User_1.User.findOne({ username: username });
            return { username: user.username, number: user.number, name: user.name, isTeacher: user.isTeacher };
        });
    }
    register(username, password, number, name, secretTeacher) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield auth_1.register(username, password, number, name, secretTeacher);
        });
    }
    login(username, password, rememberMe, { res }) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield auth_1.login(username, password);
            jwt_1.sendRefreshToken(res, jwt_1.createRefreshToken(user, rememberMe));
            return {
                accessToken: jwt_1.createAccessToken(user)
            };
        });
    }
    logout({ res }) {
        return __awaiter(this, void 0, void 0, function* () {
            jwt_1.sendRefreshToken(res, "");
            return true;
        });
    }
};
__decorate([
    type_graphql_1.Query(() => String),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], Resolvers.prototype, "hello", null);
__decorate([
    type_graphql_1.Query(() => [User_1.User]),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], Resolvers.prototype, "users", null);
__decorate([
    type_graphql_1.Query(() => MeResponse),
    type_graphql_1.UseMiddleware(auth_2.isAuthenticated),
    __param(0, type_graphql_1.Ctx()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], Resolvers.prototype, "me", null);
__decorate([
    type_graphql_1.Mutation(() => Boolean),
    __param(0, type_graphql_1.Arg("username")),
    __param(1, type_graphql_1.Arg("password")),
    __param(2, type_graphql_1.Arg("number")),
    __param(3, type_graphql_1.Arg("name")),
    __param(4, type_graphql_1.Arg("secretTeacher")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, Number, String, String]),
    __metadata("design:returntype", Promise)
], Resolvers.prototype, "register", null);
__decorate([
    type_graphql_1.Mutation(() => LoginResponse),
    __param(0, type_graphql_1.Arg("username")),
    __param(1, type_graphql_1.Arg("password")),
    __param(2, type_graphql_1.Arg("rememberMe")),
    __param(3, type_graphql_1.Ctx()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, Boolean, Object]),
    __metadata("design:returntype", Promise)
], Resolvers.prototype, "login", null);
__decorate([
    type_graphql_1.Mutation(() => Boolean),
    __param(0, type_graphql_1.Ctx()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], Resolvers.prototype, "logout", null);
Resolvers = __decorate([
    type_graphql_1.Resolver()
], Resolvers);
exports.Resolvers = Resolvers;
//# sourceMappingURL=Resolvers.js.map