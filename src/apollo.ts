import { ApolloServer } from "apollo-server-express";
import { buildSchema } from "type-graphql";
import { Resolvers } from "./Resolvers";
export default async () => {
  return new ApolloServer({
    schema: await buildSchema({
      resolvers: [Resolvers]
    }),
    context: ({ req, res }) => ({
      req,
      res
    })
  });
};
