import { User } from "./entity/User";
import {
  Resolver,
  Query,
  Mutation,
  Arg,
  Field,
  ObjectType,
  Ctx,
  UseMiddleware
} from "type-graphql";
import { register, login } from "./utils/auth";
import {
  createAccessToken,
  sendRefreshToken,
  createRefreshToken
} from "./utils/jwt";
import { isAuthenticated } from "./middleware/graphql/auth";
@ObjectType()
class LoginResponse {
  @Field()
  accessToken: string;
}
@ObjectType()
class MeResponse {
  @Field()
  username: string;
  @Field()
  name: string;
  @Field()
  number: number;
  @Field()
  isTeacher: boolean;
}

@Resolver()
export class Resolvers {
  @Query(() => String)
  hello() {
    return "hello";
  }

  @Query(() => [User])
  users() {
    return User.find();
  }

  @Query(() => MeResponse)
  @UseMiddleware(isAuthenticated)
  async me(
    @Ctx() { payload }: any
  ) {
    const { username } = payload;
    const user = await User.findOne({ username: username }) as any
    return { username: user.username, number: user.number, name: user.name, isTeacher: user.isTeacher };
  }

  @Mutation(() => Boolean)
  async register(
    @Arg("username") username: string,
    @Arg("password") password: string,
    @Arg("number") number: number,
    @Arg("name") name: string,
    @Arg("secretTeacher") secretTeacher: string
  ) {
    return await register(username, password, number, name, secretTeacher);
  }
  @Mutation(() => LoginResponse)
  async login(
    @Arg("username") username: string,
    @Arg("password") password: string,
    @Arg("rememberMe") rememberMe: boolean,
    @Ctx() { res }: any
  ) {
    const user = await login(username, password);

    sendRefreshToken(res, createRefreshToken(user, rememberMe));
    return {
      accessToken: createAccessToken(user)
    };
  }
  @Mutation(() => Boolean)
  async logout(@Ctx() { res }: any) {
    sendRefreshToken(res, "");
    return true;
  }
}
