require('dotenv').config()
import "reflect-metadata";
import express from "express";
import cors from "cors";
import cookieParser from "cookie-parser";
import { verify } from "jsonwebtoken";
import { createConnection } from "typeorm";
import { User } from "./entity/User";
import apollo from "./apollo";
import {
  createRefreshToken,
  createAccessToken,
  sendRefreshToken
} from "./utils/jwt";
const PORT = process.env.PORT || 5000;
(async () => {
  const app = express();
  app.use(cookieParser());
  app.get("/", (_, res) => res.send("home"));
  await createConnection();
  app.use(
    cors({
      origin: "http://localhost:3000",
      credentials: true
    })
  );
  app.post("/refresh_token", async (req, res) => {
    if (!req.cookies) {
      return res.send({ ok: false, accessToken: "" });
    }
    const token = req.cookies.jid;
    if (!token) {
      return res.send({ ok: false, accessToken: "" });
    }

    let payload: any = null;
    try {
      payload = verify(
        token,
        process.env.refreshTokenSecret || "testingRefresh"
      );
    } catch (err) {
      console.log(err);
      return res.send({ ok: false, accessToken: "" });
    }

    // token is valid and
    // we can send back an access token
    const user = await User.findOne({ id: payload.userId });

    if (!user) {
      return res.send({ ok: false, accessToken: "" });
    }

    /*if (user.tokenVersion !== payload.tokenVersion) {
      return res.send({ ok: false, accessToken: "" });
    }*/

    sendRefreshToken(res, createRefreshToken(user, false));

    return res.send({ ok: true, accessToken: createAccessToken(user) });
  });
  const apolloServer = await apollo();
  apolloServer.applyMiddleware({ app, cors: false, path: "/api" });
  app.listen(PORT, () => {
    console.log(`Express server started on port ${PORT} 🚀`);
  });
})();
