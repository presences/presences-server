import * as argon2 from "argon2";
import { User } from "../entity/User";
const register = async (username: string, password: string, number: number, name: string, secretTeacher: string) => {
  const user = await User.findOne({ username });
  if (user) {
    throw new Error("Username already taken");
  }
  try {
    const passwordHash = await argon2.hash(password);
    await User.insert({
      username,
      password: passwordHash,
      name,
      number,
      isTeacher: secretTeacher === 'magic'
    });
    console.log(`auth/register: ${username} created`);
    return true;
  } catch (e) {
    console.log("auth/register: ", e);
    return false;
  }
};

const login = async (username: string, password: string) => {
  const user = await User.findOne({ username });
  if (!user) {
    throw new Error("Wrong Credentials");
  }
  const comparePasswords = await argon2.verify(user.password, password);
  if (!comparePasswords) {
    console.log(`auth/login: ${user.username} attempted to log in`);
    throw new Error("Wrong Credentials");
  }

  console.log(`auth/login: ${user.username} logged in`);
  return user;
};

export { register, login };
