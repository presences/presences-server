import { Response } from "express";

import { User } from "../entity/User";
import { sign } from "jsonwebtoken";

const createAccessToken = (user: User) => {
  return sign(
    { userId: user.id, username: user.username, name: user.name },
    process.env.accessTokenSecret || "testingAccess",
    {
      expiresIn: "15m"
    }
  );
};

const createRefreshToken = (user: User, rememberMe: boolean) => {
  return sign(
    {
      userId: user.id,
      username: user.username,
      name: user.name,
      tokenVersion: 1
    }, //1 = user.tokenVersion
    process.env.refreshTokenSecret || "testingRefresh",
    {
      expiresIn: rememberMe ? "365d" : "7d"
    }
  );
};

const sendRefreshToken = (res: Response, token: string) => {
  res.cookie("jid", token, {
    httpOnly: true,
    path: "/refresh_token"
  });
};

export { createRefreshToken, createAccessToken, sendRefreshToken };
